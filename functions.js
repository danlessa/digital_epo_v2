function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}


function processFile(){

  var speed_perc = parseFloat(document.getElementById("speed_input").value);
  var hr_perc = parseFloat(document.getElementById("hr_input").value);
  var climb_perc = parseFloat(document.getElementById("climb_input").value);
  var time_perc = 1 / (speed_perc / 100 + 1);


  let file = document.getElementById('file_input').files[0];
  let filename = "digitalepo_ " + file.name;
  let reader = new FileReader();
  reader.onload = function(e){
    raw_text = reader.result;

    parser = new DOMParser();
    xmlDoc = parser.parseFromString(raw_text, "text/xml");

    if (raw_text.includes("http://www.topografix.com/GPX/1/1")){
      output = processGPX(xmlDoc, time_perc, climb_perc, hr_perc);
      download(filename, output);
    }
    else if (raw_text.includes('xmlns="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2"')){
      output = processTCX(xmlDoc, time_perc, climb_perc, hr_perc);
      download(filename, output);
    }
    else {
      console.log("invalid file");
    }
  }
  reader.readAsText(file, "UTF-8");
  return null;
}





function processGPX(doc, time_perc, climb_perc, hr_perc){
  var trackpoints = xmlDoc.getElementsByTagName("trkpt");
  var N = trackpoints.length;

  var start_trackpoint = trackpoints[0];

  var start_time = start_trackpoint.getElementsByTagName("time")[0].textContent;
  starttime_ms = Date.parse(start_time);

  var change_perc = 1 - (time_perc - 1);
  var elevation_perc = climb_perc;
  var last_elevation = parseFloat(start_trackpoint.getElementsByTagName("ele")[0].textContent);


  for (let i = 1; i < N; ++i){
    let trackpoint = trackpoints[i];
    let time = trackpoint.getElementsByTagName("time")[0].textContent;
    let elevation = trackpoint.getElementsByTagName("ele")[0].textContent;
    let numeric_elevation = parseFloat(elevation);
    let delta_elevation = numeric_elevation - last_elevation;

    let new_elevation = last_elevation + delta_elevation * elevation_perc;
    elevation = new_elevation.toFixed(1);
    last_elevation = numeric_elevation;



    let time_ms = Date.parse(time);
    let interval_ms = time_ms - starttime_ms;
    let new_interval_ms = interval_ms * change_perc;
    let newtime_ms = starttime_ms + new_interval_ms;
    let date = new Date(newtime_ms);
    date_string = date.toISOString();
    trackpoint.getElementsByTagName("time")[0].textContent = date_string;

    console.log(trackpoint);
  }

  var s = new XMLSerializer();
  var new_doc = s.serializeToString(doc);
  return(new_doc);
}

function processTCX(doc, time_perc, climb_perc, hr_perc){
  return null;
}


window.onload=function(){
  document.getElementById("get_epo_button").addEventListener("click", processFile);

    $('#epo_form').submit(function () {
   return false;
});
}
